def CYTOEmbedder(	in: CSV,
					tsne: Boolean,
					umap: Boolean,
					perplexity: Int,
					theta: Double,
					nNeighbors: Int,
					minDist: Double,
					sampling: Boolean,
					sampleSize: Int,
					num_threads: Int,
					colsToUse: String): (CSV, CSV) =
{
	import org.anduril.runtime._
	import anduril.tools._

	// Remove duplicates
	val noDuplicates = CSVTransformer(
		csv1=in,
		transform1="csv <- csv1[!duplicated(csv1), ]"
	)

	val numRows = iterCSV(noDuplicates).size

	// Downsampling based on input
	val dataDownsample = if (numRows >= sampleSize && sampling){
		RandomSampler(
		   in=noDuplicates,
		   columnFraction=true,
		   rowFraction=false,
		   numRows=sampleSize,
		   _name="dataDownsample"
	   )
   } else {
	   if (sampling){
		   	info("Given sample size is larger than the number of rows in the input data:"+numRows+" < "+sampleSize)
	   }
	   info("Using the full input data for the embedding.")
	   CSVTransformer(
		   csv1=noDuplicates,
		   transform1="csv1",
		   _name="dataDownsample"
	   )
   }

	// Data to use for the dimensionality reduction
   val dataToUse = if (colsToUse != ""){
		CSVFilter(
			in=dataDownsample,
			includeColumns=colsToUse,
			_name="dataToUse"
		)
   } else {
	   CSVTransformer(
		   csv1=dataDownsample,
		   transform1="csv1",
		   _name="dataToUse"
	   )
   }

	val tsneEmbedding = if (tsne) {
		TSNE(
		  in = dataToUse,
		  check_duplicates = false,
		  cost_tol = 1.48e-8,
		  dims = 2,
		  entropy_fast = true,
		  initial_dims = 50,
		  is_distance = false,
		  max_iter = 1000,
		  pca = false,
		  perplexity = perplexity,
		  seed = -1,
		  theta = theta,
		  verbose = true,
		  num_threads = num_threads,
		  _name="tsneEmbedding"
	  ).out
	} else { StringInput("RowName\tColumn1\tColumn2\n", _name="tsneEmbedding").out }

	val umapEmbedding = if (umap) {
		UMAP(
			in=dataToUse,
			n_neighbors=nNeighbors,
			min_dist=minDist,
			n_components=2,
			learning_rate=0.01,
			random_state=1234,
			metric="correlation",
			_name="umapEmbedding"
		).out
	} else { StringInput("RowName\tColumn1\tColumn2\n", _name="umapEmbedding").out }

	// Bind results
	val data2D = REvaluate(
		script=StringInput("""
			if(nrow(table2) != 0){
				table1 <- do.call(cbind, list(table1, table2))
			}
			if(nrow(table3) != 0){
				table1 <- do.call(cbind, list(table1, table3))
			}
			table.out <- table1
			"""),
		table1=dataDownsample,
		table2=tsneEmbedding,
		table3=umapEmbedding
	).table

	// only coordinates
	val coords = REvaluate(
		script=StringInput("""
			table <- 0
			if(nrow(table1) != 0 && nrow(table2) != 0){
				table <- do.call(cbind, list(table1, table2))
			} else if(nrow(table1) != 0 && nrow(table2) == 0){
				table <- table1
			} else if(nrow(table1) == 0 && nrow(table2) != 0){
				table <- table2
			}
			table.out <- table
			"""),
		table1=tsneEmbedding,
		table2=umapEmbedding
	).table

	return (data2D, coords)
}
