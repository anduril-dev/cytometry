def CYTOClusterer(in: CSV, method: String, k_clusters: Int, k_neighbors: Int): (CSV, CSV) = {
  	import org.anduril.runtime._
    import anduril.tools._

    val clusteringResult = if(method == "Phenograph"){
        PhenographClusterer(
            in 				 = in,
            metric           = "euclidean",
            idColumn         = "",
            columnsToRemove  = "",
            k                = k_neighbors,
            directed         = false,
            prune            = false,
            jaccard          = true,
            minClusterSize   = 10,
            nJobs            = -1,
            qTol             = 0.001,
            louvainTimeLimit = 2000,
            _name 			 = "clusters"
        ).out
    } else if(method == "FlowSOM"){
        FSomMetaClustering(
            in 				= in,
            columnsToRemove = "",
            k               = k_clusters,
            _name			= "clusters"
        ).out
    } else if(method == "X-shift"){
        XShiftClusterer(
            in 				= in,
            columns         = "",
            distance        = "angular",
            kFrom           = 150,
            kTo             = 5,
            kStep           = -30,
            n               = -1,
            _name 			= "clusters"
        ).out
    } else if(method == "k-means"){
        WekaClusterer(
            in				= in,
            method			= "SimpleKMeans",
            wekaParameters	= "-N ".concat(k_clusters.toString),
            _name			= "clusters"
        ).out
    } else if(method == "Flow-means"){
        FlowMeans(
            in				= in,
            maxN			= -1,
            numC			= -1,
            nStart			= 10,
			mahalanobis		= false,
            _name			= "clusters"
        ).out
    } else{
        StringInput("RowName\tColumn1\tColumn2\n", _name = "clusters").out
    }

    // Change cluster columnname from different clusterers to 'label'
    val fullData = REvaluate(
		script=StringInput(content="""
			table1 <- cbind(table1, table2)
			if('PhenoCluster' %in% colnames(table1)){
				idx <- which(names(table1)=='PhenoCluster')
				table1[, idx] <- table1[, idx] + 1 # clusters start from zero with phengraph so add one
				colnames(table1)[idx] <- 'label'
				table1 <- table1[, -which(colnames(table1)=='index')]
			} else if('clusterProbs' %in% colnames(table1)){
				colnames(table1)[which(names(table1)=='clusterId')] <- 'label'
				table1 <- table1[, -c(which(colnames(table1)=='id'), which(colnames(table1)=='clusterProbs'), which(colnames(table1)=='index'))]
			}
			table.out <- table1
		"""),
		table1=in,
        table2=clusteringResult
	).table

    val cluster_labels = CSVFilter(in=fullData, includeColumns="label")

    return (fullData, cluster_labels)
}
