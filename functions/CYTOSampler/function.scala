def CYTOSampler(in: CSV, sampling: Boolean, method: String, size: Int): (CSV, CSV) = {
    // in should be an array of CSV eitherway

    val sampledArray = if(method == "random" && sampling) {
        CellSampler(
            in=in,
            max=size).out
    } else if(method == "density" && sampling){
        SPADESampler(
            in=in,
            //kernelMult=2.0,
            //medSamples=2000,
            targetNumber = size,
            targetPercent = -1.0 ).out
    } else {
        in
    }

    val downsampledCSV = CSVListJoin(in=sampledArray.force(), fileCol="sample")

    return (sampledArray.force(), downsampledCSV)
}
