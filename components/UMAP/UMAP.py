import anduril
import pandas
import umap
from anduril.args import *

df = anduril.table.PandasReader(input_in)
codes = umap.UMAP(n_neighbors=param_n_neighbors,
                  min_dist=param_min_dist,
                  n_components=param_n_components,
                  learning_rate=param_learning_rate,
                  init=param_init,
                  verbose=param_verbose,
                  random_state=param_random_state,
                  metric=param_metric).fit_transform(df)

cols = ["UMAP{}".format(i) for i in range(1, param_n_components+1)]
anduril.table.PandasWriter(pandas.DataFrame(codes, columns=cols), output_out)
