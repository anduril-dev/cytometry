library(componentSkeleton)
library(flowCore)
library(spade)
# Add here other libraries you will need

execute <- function(cf) {
# Read INPUT ports:
write.log(cf, "Reading input files from command file ...")
input.fcs <- get.input(cf, 'in')
# Read OUTPUT ports:'
write.log(cf, "Reading output files from command file ...")
output.folder <- get.output(cf, 'out')

plot.dir <- get.output(cf, 'treePlots')
if (!file.exists(plot.dir)) {
dir.create(plot.dir, recursive=TRUE)
}

# Read parameters:
write.log(cf, "Reading parameters from command file ...")

median_cols = NULL
fold_cols <- get.parameter(cf, "fold_cols", "string")
transforms <- get.parameter(cf, "transforms", "string")
downsampling_target_percent <- get.parameter(cf, "downsampling_target_percent", "float")
downsampling_target_number <- get.parameter(cf, "downsampling_target_number", "float")
downsampling_target_pctile <- get.parameter(cf, "downsampling_target_pctile", "float")
downsampling_exclude_pctile <- get.parameter(cf, "downsampling_exclude_pctile", "float")
k <- get.parameter(cf, "k", "int")
clustering_samples <- get.parameter(cf, "clustering_samples", "int")
file_pattern <- get.parameter(cf, "file_pattern", "string")
size_scale_factor <- get.parameter(cf, "size_scale_factor", "float")


# Build cytoSPADE trees with driver function
write.log(cf, "...")

b <- read.FCS(input.fcs)
markers = colnames(b)

if (input.defined(cf, "reference")) {
	reference <- get.input(cf, "reference")
	PANELS <- list(list(panel_files=c("in"), median_cols=NULL,reference_files=c(reference),fold_cols=c()))
} else {
	PANELS <- list(list(panel_files=c("in"), median_cols=NULL,reference_files=c("in"),fold_cols=c()))
}


SPADE.driver(input.fcs, out_dir=output.folder, cluster_cols=markers, panels=PANELS, transforms=flowCore::arcsinhTransform(a=0, b=0.2), layout=SPADE.layout.arch,
             downsampling_target_percent=downsampling_target_percent, downsampling_target_number=downsampling_target_number,
             downsampling_target_pctile=downsampling_target_pctile, downsampling_exclude_pctile=downsampling_exclude_pctile,
             k=k, clustering_samples=clustering_samples)

# Create PDFs of the SPADE trees
layout <- read.table(file.path(output.folder,"layout.table"))
mst <- read.graph(file.path(output.folder,"mst.gml"),format="gml")
SPADE.plot.trees(mst,output.folder,file_pattern="*fcs*Rsave",layout=as.matrix(layout),out_dir=file.path(output.folder,"pdf"),size_scale_factor=1.2)

# Take result files from output folder
#from <- dir("tmp/tables/bySample/", full.names = TRUE)
#file.rename( from = from , to = out ) 

#file.copy( from = "tmp/pdf" , to = plot.dir , recursive = TRUE)

#unlink("tmp", recursive = TRUE)
return(0)
}

main(execute)
