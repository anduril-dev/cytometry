<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>FCSTransform</name>
    <version>1.0</version>
    <doc>
    Cell fluorescense intensity transformation for FACS data. The user can use the transformation plots to check that the transformation looks correct. Ideally it should make the data look like a blob, if dots are aligned to the zero in either of the axis or are placed below zero would mean that the transformation is not separating the values properly and another transformation should be used.
    </doc>
    <author email="julia.casado@helsinki.fi">Julia Casado</author>
    <category>Single Cell Preprocessing</category>
    <category>Cytometry</category>
    <launcher type="R">
        <argument name="file" value="FCSTransform.r" />
    </launcher>
    <requires>R</requires>
    <requires type="R-bioconductor">flowCore</requires>
    <requires type="R-bioconductor">openCyto</requires>
    <inputs>
        <input name="in" type="BinaryFolder">
            <doc>A folder of raw FCS files with all the data for a given experiment.</doc>
        </input>
    </inputs>
    <outputs>
        <output name="out" type="BinaryFolder">
            <doc>Transformed data in folder of FCS files.</doc>
        </output>
    </outputs>
    <parameters>
	<parameter name="channels" type="string" default="*">
		<doc>A comma-separated list of channel numbers indicating which
		     columns to transform. Antibody names that match partially the names of the columns. The default is for all. If empty, it will not do any transformation. If parameter 'negate' is true then all channels not in this list are transformed.
		</doc>
	</parameter>
	<parameter name="negate" type="boolean" default="false">
		<doc>If true, the transformation will be applied to all channels except those defined in the 'channels' parameter.
		</doc>
	</parameter>
	<parameter name="transformation" type="string" default="arcsinhTransform(transformationId='defaultArcsinhTransform', a=1, b=2)">
		<doc>The transformation method to apply, open definition of an object of type transform. Examples:&#xD;
		linearTransform(transformationId="defaultLinearTransform", a = 1, b = 0)&#xD;
		lnTransform(transformationId="defaultLnTransform", r=1, d=1)&#xD;
		logicleTransform(transformationId="defaultLogicleTransform", w = 0.5, t = 262144, m = 4.5, a = 0)&#xD;
		biexponentialTransform(transformationId="defaultBiexponentialTransform", a = 0.5, b = 1, c = 0.5, d = 1, f = 0, w = 0, tol = .Machine$double.eps^0.25, maxit = as.integer(5000))&#xD;
		arcsinhTransform(transformationId="defaultArcsinhTransform", a=1, b=1, c=0)&#xD;
		quadraticTransform(transformationId="defaultQuadraticTransform", a = 1, b = 1, c = 0)&#xD;
		logTransform(transformationId="defaultLogTransform", logbase=10, r=1, d=1)&#xD;
		scaleTransform(transformationId="defaultScaleTransform", a, b)&#xD;
		truncateTransform(transformationId="defaultTruncateTransform", a=1)&#xD;
		</doc>
	</parameter>
    </parameters>
</component>