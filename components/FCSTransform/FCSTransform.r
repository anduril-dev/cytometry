# Loading the required libraries.
suppressPackageStartupMessages(
{
    packages = c( "componentSkeleton", "flowCore", "openCyto" )
    lapply( packages, require, character.only = TRUE )
})

flowSet.to.df <- function( fs ) {
    samples <- sampleNames( fs )
    df.list <- lapply( samples, function( x ) cbind( File = rep( x, nrow( fs[[ x ]] ) ), exprs( fs[[ x ]] ) ) )
    df <- do.call( rbind, df.list )
    return( as.data.frame( df ) )
}

execute <- function( cf ) {
    # -------------------------------------- #
    #  Read data into a flowSet object
    # -------------------------------------- #
    ## TODO:
    # Try read.flowSet
    raw.fs <- read.flowSet( list.files( get.input( cf, 'in' ),full.names=T ) )
    # if fails, read.FCS one by one and check that channels are in the same order and same names
    # throw error
    #raw.fs <- lapply( list.files( get.input( cf, 'in' ),full.names=T ), read.FCS )

    # -------------------------------------- #
    #  Process parameters
    # -------------------------------------- #
    # Read parameters: negate parameter to invert channel selection
    negate <- get.parameter( cf, 'negate', 'boolean' )
    # Read parameters: channels list: TODO: numeric or character?
    channels <- get.parameter( cf, 'channels', 'string' )
    # Make list of channels to be transformed based on parameters
    cnames <- colnames( raw.fs[[1]] )
    if( ( channels == "*" && !negate ) || ( channels == "" && negate ) )   {
        # All channels
        channels <- cnames
    } else if( ( channels == "*" && negate ) || ( channels == "" && !negate ) )  {
        # None -> Do not transform anything!
        write.error( "No channels selected for transformation. Component will do nothing." )
        return( 1 )
    } else {
        # Partial list
        channels <- unlist( strsplit( channels, ',' ) )
        if (sum( !is.na(as.numeric(channels)) )) {
            if ( max( as.numeric(channels) )  > length( cnames ) ) {
                write.error( cf, 'Channel indices out of bounds' )
                return( 1 )
            }
            channels.idx.list <- as.numeric(channels)
        } else {
            channels.idx.list <- unlist( lapply( channels, grep, cnames ) )
        }
        if( !negate ) {
            channels <- cnames[ channels.idx.list ]
        } else {
            channels <- cnames[ -channels.idx.list ]
        }
    }

    # Read parameters: transformation method of choice (trying open API)
    method <- eval( parse( text = get.parameter( cf, 'transformation', 'string' ) ) )
    # Test that method is indeed of class 'transform'
    if ( !inherits( method,"transform" ) ) {
        write.error( cf, paste( 'Parameter transformation:', method, 'is not a correct R command defining an object of class transform') )
        return( 1 )
    }

    # -------------------------------------- #
    #  Transform data
    # -------------------------------------- #
    # Set transformation in the selected channels
    transform.list <- transformList( channels, method )
    # Transform the flowSet
    transformed.fs <- transform( raw.fs, transform.list )

    # -------------------------------------- #
    #  Write output array of CSV
    # -------------------------------------- #
    out.dir <- get.output(cf, 'out')
    if (!file.exists(out.dir)) {
        dir.create(out.dir, recursive=TRUE)
    }
    write.flowSet( transformed.fs, outdir=out.dir )

	return(0)
}

main(execute)

## NOTES:

    #‘linearTransform’: The definition of this function is currently x <- a*x+b
            # linearTransform(transformationId="defaultLinearTransform", a = 1, b = 0)
    #‘lnTransform’:  x<-log(x)*(r/d)
            # lnTransform(transformationId="defaultLnTransform", r=1, d=1)
    #‘logicleTransform’:
            # logicleTransform(transformationId="defaultLogicleTransform", w = 0.5, t = 262144, m = 4.5, a = 0)
    #‘biexponentialTransform’: biexp(x) = a*exp(b*(x-w))-c*exp(-d*(x-w))+f
            # biexponentialTransform(transformationId="defaultBiexponentialTransform", a = 0.5, b = 1, c = 0.5, d = 1, f = 0, w = 0, tol = .Machine$double.eps^0.25, maxit = as.integer(5000))
    #‘arcsinhTransform’: x<-asinh(a+b*x)+c)
            # arcsinhTransform(transformationId="defaultArcsinhTransform", a=1, b=1, c=0)
    #‘quadraticTransform’: x <- a*x\^2 + b*x + c
            # quadraticTransform(transformationId="defaultQuadraticTransform", a = 1, b = 1, c = 0)
    #‘logTransform’: x<-log(x,logbase)*(r/d)
            # logTransform(transformationId="defaultLogTransform", logbase=10, r=1, d=1)
    # scaleTransform: x = (x-a)/(b-a)
            # scaleTransform(transformationId="defaultScaleTransform", a, b)
    # truncateTransform: x[x<a] <- a
            # truncateTransform(transformationId="defaultTruncateTransform", a=1)