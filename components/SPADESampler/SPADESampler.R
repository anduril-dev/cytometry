
library(componentSkeleton)
library(Rcpp)
# library("flowCore", lib.loc = "../../lib/spade")
# library("spade", lib.loc = "../../lib/spade")
library(flowCore)
suppressPackageStartupMessages(library(spade))

get.nullable.parameter <- function(cf, name, type) {
	# this converts -1s into NULL for "optional" parameters
	value <- get.parameter(cf, name, type = type)
	if (value == -1)
		value <- NULL

	return (value)
}

set.c.seed <- cppFunction("
	void set_c_seed(int seed) {
		std::srand(unsigned(seed));
	}
")

is.fcs <- function(filename) {
	# check magic for FCS file
	return (readChar(filename, 3) == 'FCS')
}

csv.to.fcs <- function(dest, src) {
	# convert a CSV file into an FCS
	data <- read.table(src, sep = '\t', header = T)
	suppressWarnings(write.FCS(flowFrame(as.matrix(data)), dest))

	# read back the data & check
	new.table <- read.FCS(dest)
	if (any(abs(data - exprs(new.table)) > 1e-6 * abs(data)))
		warning(sprintf('%s: the internal CSV to FCS conversion lost precision', src))

	return (dest)
}

fcs.to.csv <- function(dest, src) {
	# convert an FCS file back to a CSV
	table <- read.FCS(src)
	data <- exprs(table)
	colnames(data) <- colnames(table)
	data <- data[, -which(colnames(data)=="density")]
	write.table(data, dest, sep = '\t', col.names = T, row.names = F)

	return (dest)
}

execute <- function(cf) {
	# get parameters
	seed <- get.nullable.parameter(cf, 'seed', type = 'int')
	kernel_mult <- get.nullable.parameter(cf, 'kernelMult', type = 'float')
	apprx_mult <- get.nullable.parameter(cf, 'apprxMult', type = 'float')
	med_samples <- get.nullable.parameter(cf, 'medSamples', type = 'int')
	exclude_pctile <- get.nullable.parameter(cf, 'excludePctile', type = 'float')
	target_pctile <- get.nullable.parameter(cf, 'targetPctile', type = 'float')
	target_number <- get.nullable.parameter(cf, 'targetNumber', type = 'int')
	target_percent <- get.nullable.parameter(cf, 'targetPercent', type = 'float')

	# create output directory
	out.dir <- get.output(cf, 'out')
	if (!file.exists(out.dir)) {
		dir.create(out.dir, recursive = T)
	}

	# generate a temporary filename
	tmp.fn <- tempfile(tmpdir = get.temp.dir(cf))

	# create output array
	out.array <- Array.new()
    tmp.array <- list()
	
	# loop through the inputs
	in.array <- Array.read(cf, 'in')
	for (i in 1:Array.size(in.array)) {
		# get key/filename
		key <- Array.getKey(in.array, i)
		in.fn <- Array.getFile(in.array, key)
    
		# if not an FCS file, convert
		was.fcs <- is.fcs(in.fn)
		spade.in.fn <- in.fn
		if (!was.fcs)
			spade.in.fn <- csv.to.fcs(tmp.fn, in.fn)

		# derive output filename
		out.stem <- sprintf('%s.%s', gsub(".csv|.fcs","", key), c('csv', 'fcs')[was.fcs + 1])
		out.fn <- file.path(out.dir, out.stem)
		out.array <- Array.add(out.array, key, out.stem)

		# get output filename for SPADE
		spade.out.fn <- out.fn
		if (!was.fcs)
			spade.out.fn <- tmp.fn

		# seed PRNG
		if (!is.null(seed)) {
			set.c.seed(seed)
			set.seed(seed)
		}

		ncells <- NULL
		if (!is.null(target_number) & is.null(target_percent)) {
				ncells <- min(round(target_number/Array.size(in.array)), nrow(spade.in.fn))
		}
		
		# downsample data
		SPADE.addDensityToFCS(spade.in.fn, spade.out.fn,
			kernel_mult = kernel_mult, apprx_mult = apprx_mult,
				med_samples = med_samples)
		SPADE.downsampleFCS(spade.out.fn, spade.out.fn, # NB. yes, use out.fn as temp file
			exclude_pctile = exclude_pctile, target_pctile = target_pctile,
				target_number = ncells, target_percent = target_percent)

		# convert data back
		if (!was.fcs)
			fcs.to.csv(out.fn, spade.out.fn)
		
		tmp.array[[key]] <- CSV.read(out.fn)
		
	}
	
	# write the full annotated data to one CSV file
	samples <- names(tmp.array)
	df.list <- lapply(samples, function(x) cbind(sample = rep(gsub(".csv|.fcs","", x), times=nrow(tmp.array[[x]])), tmp.array[[x]]))
	df <- do.call(rbind, df.list)
	
	# Drop the density column 
	df <- df[, -which(colnames(df)=="density")]
	CSV.write(get.output(cf, 'outCSV'), df)

	# write output arary table
	Array.write(cf, out.array, 'out')
	


	return (0)
}

main(execute)
