#!/bin/bash
set -e

Rscript -e 'install.packages("devtools"); library(devtools); devtools::install_github("nolanlab/Rclusterpp",force=TRUE);install.packages("BiocManager"); devtools::install_github("nolanlab/spade")'