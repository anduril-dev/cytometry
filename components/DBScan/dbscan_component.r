library(componentSkeleton)
library(dbscan)


execute <- function(cf) {
    # Read input location
    input.csv <- get.input(cf,'in')
    
    # Get parameters and parse them.
    channels.to.cluster <- get.parameter(cf, 'channelsToCluster')
    # TODO: eps can be approximated. have a default value 0 for approximation.
    eps          <- get.parameter(cf, 'eps', type='float')
    minPts       <- get.parameter(cf, 'minPts', type='int')
    weightsCol   <- get.parameter(cf, 'weights')
    borderPoints <- get.parameter(cf, 'borderPoints', type='boolean')
    search       <- get.parameter(cf, 'search')
    bucketSize   <- get.parameter(cf, 'bucketSize', type='int')
    splitRule    <- get.parameter(cf, 'splitRule')
    approx       <- get.parameter(cf, 'approx', type='float')
    cluster.id.col.name <- get.parameter(cf, 'clusterIDColName')

    output.clusters <- get.output(cf, 'out')
    csv <- CSV.read(input.csv)
    if(channels.to.cluster == "*")  {
        channels.to.cluster <- colnames(csv)
    } else {
        channels.to.cluster <- split.trim(channels.to.cluster, ",")
    }
    weights<-NULL    
    if(weightsCol != "") {
        weights<-csv[weightCol,]
    }
    ## Cluster the data.
    col.names <- channels.to.cluster[channels.to.cluster %in% colnames(csv)]
    col.names <- col.names[col.names!=weightsCol]
    csv.cluster<- as.matrix(csv[,col.names])
    if(cluster.id.col.name %in% colnames(csv)) {
        write.error(cf, sprintf('clusterIDColName %s is already present in the input file %s.', cluster.id.col.name, input.file.name))
    }
    clust.res <- dbscan(csv.cluster, 
                        eps=eps,
                        minPts=minPts,
                        weights=weights,
                        borderPoints=borderPoints,
                        search=search,
                        bucketSize=bucketSize,
                        splitRule=splitRule,
                        approx=approx
                    )
    print(clust.res)
    res.csv <- csv
    res.csv[,cluster.id.col.name] <- clust.res$cluster
    CSV.write(output.clusters, res.csv)

    
}

main(execute)
