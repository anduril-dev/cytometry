library( componentSkeleton )

execute <- function( cf ) {
    # Read input array of CSV
    in.array <- Array.read( cf, 'in' )
    array <- list()
    for( i in 1:Array.size( in.array ) ) {
        key <- Array.getKey( in.array, i )
        file <- Array.getFile( in.array, key )
        array[[ key ]] <- CSV.read( file )
    }
    # Read parameters: max and method
    max.cells <- get.parameter(cf, 'max', type="int")
    # Read parameters: max and method
    set.seed( get.parameter(cf, 'seed', type="int") )

    # Calculate number of cells to sample per file
    ncells <- unlist(lapply( array, function(x) min( round(max.cells/length(array) ), nrow(x) )))
    names(ncells) <- names(array)

    
    array.out.dir <- get.output(cf, 'out')
    if (!file.exists(array.out.dir)) {
        dir.create(array.out.dir, recursive=TRUE)
    }

    array.out.object <- Array.new()
    array.out <- list()
    for(key in names(array)) {
        # Actual sampling step
        idx <- sample( nrow( array[[key]]), ncells[key] )
        array.out[[key]] <- array[[key]][ idx, ]

        filename = paste(key, ".csv", sep="")
        CSV.write(paste(array.out.dir, "/", filename, sep=""), array.out[[ key ]] )
        array.out.object <- Array.add(array.out.object, key, filename)
    }

    # Write output array
    Array.write(cf, array.out.object, 'out')

    # Be happy
    return(0)
}

main(execute)
