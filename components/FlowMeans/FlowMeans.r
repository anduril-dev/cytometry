library(componentSkeleton)
library(flowMeans)

## Width of a sub figure in the report latex document.
WIDTH.OF.SUB.FIGURE = '0.3\\textwidth'

execute <- function(cf) {
    # Read input location
    input.csv <- get.input(cf,'in')
    
    # Get parameters and parse them.
    channels.to.cluster <- get.parameter(cf, 'channelsToCluster')
    if(channels.to.cluster == "*")  {
        channels.to.cluster <- NULL
    }else {
        channels.to.cluster <- split.trim(channels.to.cluster, ",")
    }    
    max.n <- get.parameter(cf, 'maxN', type='int')
    if(max.n < 0) max.n <- NA
    num.c <- get.parameter(cf, 'numC', type='int')
    if(num.c < 0) num.c <- NA
    iter.max <- get.parameter(cf, 'iterMax', type='int')
    if(iter.max < 0) write.error(cf, 'Invalid parameter value.')    
    n.start <- get.parameter(cf, 'nStart', type='int')
    if(n.start < 0) {
        write.error(cf, 'Invalid parameter value.')
    }
    mahalanobis <- get.parameter(cf, 'mahalanobis', type='boolean')
    standardize <- get.parameter(cf, 'standardize', type='boolean')
    cluster.id.col.name <- get.parameter(cf, 'clusterIDColName')
    
    # Component name in Anduril network
    instance.name <- get.metadata(cf, 'instanceName')

    # Get output directory paths and create them.
    output.clusters <- get.output(cf, 'out')
    output.full <- get.output(cf, 'outFull')
    # output.report <- get.output(cf, 'report')
    # dir.create(output.report)

    # output.report.figure.name <- sprintf("%s.pdf", instance.name)
    # Do the clustering.
    input.to.clustering <- list(input.file.name=input.csv, 
                                channels.to.cluster=channels.to.cluster, 
                                max.n=max.n, 
                                num.c=num.c, 
                                iter.max=iter.max, 
                                n.start=n.start, 
                                mahalanobis=mahalanobis, 
                                standardize=standardize, 
                                output.file.name=output.clusters,
                                output.file.full=output.full,
                                # output.report=output.report, 
                                cluster.id.col.name=cluster.id.col.name
                                # figure.name=output.report.figure.name
                                )
    cluster.flow.means(cf, input.to.clustering)
    
    ## Construct the LaTex report.
    # tex <- character()
    # tex <- c(tex, '\\begin{figure}[!ht]\\centering')
    # fig.caption <- "Selection of optimal number of clusters for samples:"
    # 
    # sub.caption <- instance.name
    # ## Subfigure label
    # sub.label <- sprintf("fig:%s", instance.name)
    # tex <- c(tex, sprintf('\\subfloat[][]{\\includegraphics[width=%s,keepaspectratio=true]{%s}\\label{%s}}', WIDTH.OF.SUB.FIGURE, output.report.figure.name, sub.label))
    # tex <- c(tex, '\\hspace{8pt}')
    # fig.caption <- paste(fig.caption, sprintf("\\subref{%s} %s", sub.label, latex.quote(sub.caption)))
    #     
    # fig.caption <- paste(fig.caption, ".", sep="")
    # tex <- c(tex, sprintf('\\caption{%s}\\label{fig:%s}', fig.caption, instance.name), '\\end{figure}')
    # latex.write.main(cf, 'report', tex)
}


# Clustering with FlowMeans.
cluster.flow.means <- function(cf, input) {
    ## Cluster the data.
    csv <- CSV.read(input$input.file.name)
    if(input$cluster.id.col.name %in% colnames(csv)) {
        write.error(cf, sprintf('clusterIDColName %s is already present in the input file %s.', input$cluster.id.col.name, input$input.file.name))
    }    
    col.names <- input$channels.to.cluster[input$channels.to.cluster %in% colnames(csv)]    
    clust.res <- flowMeans(csv, varNames=col.names, MaxN=input$max.n, NumC=input$num.c, iter.max=input$iter.max, nstart=input$n.start, Mahalanobis=input$mahalanobis)
    res.csv <- csv
    res.csv[,input$cluster.id.col.name] <- clust.res@Label
    CSV.write(input$output.file.full, res.csv)
    CSV.write(input$output.file.name, data.frame(label=clust.res@Label))

    # ## Plot change point detection.
    # temp <- unlist(strsplit(input$input.file.name, "/"))
    # temp <- temp[length(temp)]
    # sample.name <- sub("\\.csv", "", temp)
    # pdf(file=file.path(input$output.report, input$figure.name), width=5, height=5)
    # plot(clust.res@Mins, xlab = " ", ylab = " ", xlim = c(1, clust.res@MaxN), ylim = c(0, max(clust.res@Mins)))
    # ft <- changepointDetection(clust.res@Mins)
    # ## If the slope of the fitted lines is NA, it's interpreted as slope being 0. This will be plotted unlike if the slope is NA.
    # if(is.na(ft$l1$coefficients[2])) ft$l1$coefficients[2] <- 0
    # if(is.na(ft$l2$coefficients[2])) ft$l2$coefficients[2] <- 0
    # abline(ft$l1)
    # abline(ft$l2)
    # par(new = TRUE)
    # plot(ft$MinIndex + 1, clust.res@Mins[ft$MinIndex + 1], col = "red", xlab = "Iteration", ylab = "Distance", xlim = c(1, clust.res@MaxN), ylim = c(0, max(clust.res@Mins)), pch = 19, main=sample.name)
    # dev.off()
}

main(execute)
