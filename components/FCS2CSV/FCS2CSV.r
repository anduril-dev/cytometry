library( componentSkeleton )
library( flowCore )

comp <- function( cf, fcs ) {
	desc <- description( fcs )
    if( is.null( desc$SPILL ) ) {
    	write.log( cf, paste( 'No spillover matrix stored in file', key, '. Using raw values' ) )
    	return( fcs )
    }
    if( desc$`APPLY COMPENSATION` == 'TRUE' ) {
		write.log( cf, paste( 'Original data in file', key, 'is already compensated.', 'Compensating because parameter "compensate" = true' ) )
    }
    return( compensate(ff, desc$SPILL) )
}

execute <- function( cf ) {
    # Read INPUT ports: FCS array
    in.array <- Array.read( cf, 'in' )
    fcs.array <- list()
    for( i in 1:Array.size( in.array ) ) {
        key <- Array.getKey( in.array, i )
        file <- Array.getFile( in.array, key )
   		fcs.array[[ key ]] <- read.FCS( file )
    }
    # Read INPUT ports: channel annotations
    first <- fcs.array[[ 1 ]]
    if( input.defined( cf, 'annotations' ) ) {
        annotations.csv <- CSV.read(get.input( cf, 'annotations' ) )
        for( i in 1:nrow(annotations.csv) ){
            channel <- annotations.csv$Channel[i]
            label <- annotations.csv$Label[i]
            if( channel %in% colnames(first) ) {
                colnames(first)[ which( colnames(first) == channel ) ] <- label
            }else{
                write.log( cf, paste( 'Channel', channel, 'not in FCS channel names:', paste( colnames(first), collapse=', ') ) )
            }
        }
    }
    cnames <- colnames( first )
    # Parameters
    compensate.param <- get.parameter( cf, 'compensate', type='boolean' )
    # Create dir to store output array
    array.out.dir <- get.output(cf, 'out')
    if (!file.exists(array.out.dir)) {
        dir.create(array.out.dir, recursive=TRUE)
    }
    # Read each flowFrame object (FCS file) from the array
    out.array <- list()
    array.out.object <- Array.new()
    metadata.csv <- NULL
    colnames.first <- NULL
    for( key in names( fcs.array ) ) {
    	if( compensate.param ){
    		result <- comp( cf, fcs.array[[ key ]] )
    	}else{
    		result <- fcs.array[[ key ]]
    	}
    	# Store data from "key" file to the array of csv
    	fcs.data <- exprs( result )
        colnames(fcs.data) <- parameters(result)[[2]]
        colnames(fcs.data)[is.na(colnames(fcs.data))] <- colnames(result)[is.na(colnames(fcs.data))]
        #out.array[[ key ]] <- cbind( Event = 1:nrow( fcs.data ), fcs.data )
        out.array[[ key ]] <- fcs.data
        filename = paste(key, ".csv", sep="")
        

        #colnames( out.array[[ key ]] ) <- cnames
        CSV.write(paste(array.out.dir, "/", filename, sep=""), out.array[[ key ]] )
        array.out.object <- Array.add(array.out.object, key, filename)

    	# Keep header information to metadata file except SPILL matrix
    	desc <- description( result )
    	desc[[ 'SPILL' ]] <- NULL
    	names( desc ) <- unlist( lapply( names( desc ), function(x) gsub( '\\$', '', x ) ) )
    	# Check that the field names in the metadata are the same for all the files
    	if( is.null( colnames.first ) ) {
    		colnames.first <- names( desc )
    	}
    	if( !identical( colnames.first, names( desc ) ) ) {
    		write.error( cf, paste( 'Files belong to different experiments. Different metadata fields in FCS file', key ) )
    		return(1)
    	}
    	# Store metadata
    	metadata.csv <- rbind( metadata.csv, c( File=key, desc ) ) 
    }
    
    # Write output metadata file 
    CSV.write(get.output(cf, 'metadata'), metadata.csv )
    
    # Write output array
    Array.write(cf, array.out.object, 'out')

    # Be happy
    return(0)
}

main(execute)
