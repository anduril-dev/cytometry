library( componentSkeleton )
library( flowCore )

execute <- function( cf ) {
    # Read INPUT ports: CSV array
    in.array <- Array.read( cf, 'in' )
    csv.array <- list()
    for( i in 1:Array.size( in.array ) ) {
        key <- Array.getKey( in.array, i )
        file <- Array.getFile( in.array, key )
        csv.array[[ key ]] <- CSV.read( file )
    }
    # Parameters

    # Create dir to store output array
    array.out.dir <- get.output(cf, 'out')
    if (!file.exists(array.out.dir)) {
        dir.create(array.out.dir, recursive=TRUE)
    }
    # Read each CSV file from the array
    array.out.object <- Array.new()
    for( key in names( csv.array ) ) {
        csv.matrix <- as.matrix(csv.array[[ key ]])
        filename = paste(key, ".fcs", sep="")

        write.FCS(flowFrame(exprs=csv.matrix), paste(array.out.dir, "/", filename, sep=""))
        array.out.object <- Array.add(array.out.object, key, filename)
    }
    # Write output array
    Array.write(cf, array.out.object, 'out')

    # Be happy
    return(0)
}

main(execute)
