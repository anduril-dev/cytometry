import anduril.cytometry._
import anduril.tools._
import anduril.builtin._
import org.anduril.runtime._

object ClustererNetwork {

  val input = INPUT(path="in.csv")
  
  val clustered_data = NamedMap[CSV]("clustered_data")
  val clustered_labels = NamedMap[CSV]("clustered_labels")
  for (method <- List("Phenograph", "FlowSOM", "k-means", "X-shift", "Flow-means")) {
    withName(quote(method)) {
      val clustered = CYTOClusterer(
        in = input,
        method = method
      )
      clustered_labels(method) = clustered.clusters
      clustered_data(method) = clustered.out
    }
  }
  val all_labels = Array2Folder(in = makeArray(clustered_labels))
  val all_clustered = Array2Folder(in = makeArray(clustered_data))
  // OUTPUT(all_labels)
  // OUTPUT(all_clustered)
}
